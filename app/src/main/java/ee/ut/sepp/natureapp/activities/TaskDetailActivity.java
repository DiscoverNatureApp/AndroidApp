package ee.ut.sepp.natureapp.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.enums.ResultEnum;
import ee.ut.sepp.natureapp.enums.TaskStatus;
import ee.ut.sepp.natureapp.helpers.DialogHelper;
import ee.ut.sepp.natureapp.helpers.ImageHelper;
import ee.ut.sepp.natureapp.models.Result;
import ee.ut.sepp.natureapp.models.Task;
import uk.co.deanwild.flowtextview.FlowTextView;

import static android.util.Base64.DEFAULT;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.GROUP_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.TASK_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.TASK_SUBMITTED_PHOTO;
import static ee.ut.sepp.natureapp.constants.PermissionsConstants.PERMISSIONS;
import static ee.ut.sepp.natureapp.constants.PermissionsConstants.PERMISSIONS_GROUP;
import static ee.ut.sepp.natureapp.constants.PermissionsConstants.REQUEST_IMAGE_CAPTURE;

public class TaskDetailActivity extends AppCompatActivity {
    private File output = null;

    @BindView(R.id.task_title)
    TextView taskTitle;
    @BindView(R.id.desc_image)
    ImageView descImage;
    @BindView(R.id.taken_photo)
    ImageView takenPhoto;
    @BindView(R.id.answer_type)
    ImageView answerType;

    @BindView(R.id.add_picture_text)
    TextView helpingText;

    @BindView(R.id.task_description)
    FlowTextView description;
    @BindView(R.id.btn_take_picture)
    ImageButton btnTakePicture;

    @BindView(R.id.btn_save_picture)
    Button btnSavePicture;


    AppDatabase db;
    Task task;
    Bitmap submittedPhoto;
    boolean isGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);

        ButterKnife.bind(this);

        db = AppDatabase.getAppDatabase(this);

        if (getIntent().hasExtra(TASK_SUBMITTED_PHOTO) && getIntent().hasExtra(TASK_ID) && getIntent().hasExtra(GROUP_ID)){
            submittedPhoto = ImageHelper.decodeBitmap(getIntent().getStringExtra(TASK_SUBMITTED_PHOTO));
            isGroup = true;
            prepareContent(getIntent().getStringExtra(TASK_ID),getIntent().getStringExtra(GROUP_ID) );
        }
        else if (getIntent().hasExtra(TASK_ID)) {
            prepareContent(getIntent().getStringExtra(TASK_ID), getIntent().getStringExtra(GROUP_ID));
            isGroup = false;
        }

        btnSavePicture.setClickable(false);
        btnSavePicture.setAlpha(0.3f);
    }

    @OnClick(R.id.btn_take_picture)
    public void takePicture(){
        //TODO improve the quality of picture taken
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSIONS_GROUP);
            return;
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @OnClick(R.id.btn_save_picture)
    public void savePicture(){
        if (submittedPhoto != null) {
            db.taskDao().submitTakenPhoto(task.getId(), ImageHelper.encodeBitmap(submittedPhoto), TaskStatus.DONE.name());
            Toast.makeText(this, getString(R.string.task_answer_saved), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(TaskDetailActivity.this, TasksActivity.class));
        }

    }

    @OnClick(R.id.desc_image)
    public void showDescFullSize() {
        BitmapDrawable drawable = (BitmapDrawable) descImage.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        DialogHelper.showFullSizeImage(this, bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            submittedPhoto = (Bitmap) extras.get("data");
            if (submittedPhoto != null) {
                helpingText.setText(getString(R.string.your_taken_photo));
                takenPhoto.setVisibility(View.VISIBLE);
                btnTakePicture.setVisibility(View.GONE);
                takenPhoto.setImageBitmap(submittedPhoto);
                btnSavePicture.setClickable(true);
                btnSavePicture.setAlpha(1f);
                task.setSubmittedPhoto(ImageHelper.encodeBitmap(submittedPhoto));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return false;
    }

    @OnClick(R.id.taken_photo)
    public void showOptionsDialog() {
        DialogInterface.OnClickListener takePictureListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePicture();
            }
        };

        DialogInterface.OnClickListener viewPictureListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (submittedPhoto != null)
                    DialogHelper.showFullSizeImage(TaskDetailActivity.this, submittedPhoto);
            }
        };
        if (!isGroup) DialogHelper.showTakenPhotoOptionsDialog(this, viewPictureListener, takePictureListener);
        else DialogHelper.showFullSizeImage(this, submittedPhoto);
    }

    private void prepareContent(String taskId, String groupId) {
        task = db.taskDao().getTaskById(taskId);
        if (task != null) {
            setTitle(task.getTitle());
            takenPhoto.setVisibility(View.GONE);
            taskTitle.setText(task.getTitle());
            if (task.getSubmittedPhoto() != null && !isGroup)
                submittedPhoto = ImageHelper.decodeBitmap(task.getSubmittedPhoto());
            description.setText(task.getDescription());
            try{
                byte[] data = Base64.decode(task.getImageDesc(), DEFAULT);
                Glide.with(this).load(BitmapFactory.decodeByteArray(data,0, data.length)).into(descImage);
            } catch(Exception e){
                Glide.with(this).load(ImageHelper.decodeBitmap(task.getImageDesc())).into(descImage);
            }
            finally {
                if (submittedPhoto != null && isGroup){
                    Glide.with(this).load(submittedPhoto).into(takenPhoto);
                    helpingText.setText(getString(R.string.group_taken_photo));
                    setAnswerType(groupId);
                    takenPhoto.setVisibility(View.VISIBLE);
                    btnTakePicture.setVisibility(View.GONE);
                    btnSavePicture.setVisibility(View.GONE);
                    answerType.setVisibility(View.VISIBLE);
                }
                else if (task.getSubmittedPhoto() != null) {
                    Glide.with(this).load(ImageHelper.decodeBitmap(task.getSubmittedPhoto())).into(takenPhoto);
                    takenPhoto.setVisibility(View.VISIBLE);
                    helpingText.setText(getString(R.string.your_taken_photo));
                    btnTakePicture.setVisibility(View.GONE);
                    answerType.setVisibility(View.GONE);
                }
            }
        }
    }

    private void setAnswerType(String groupId) {
        if (groupId != null) {
            Result result = AppDatabase.getAppDatabase(this).resultDao().getResultOfTaskByGroup(groupId, task.getId());
            String grade = result.getGrade().toUpperCase();

            if (grade.equals((ResultEnum.CORRECT.name())))
                answerType.setImageResource(ResultEnum.CORRECT.getImageResource());
            else if (grade.equals(ResultEnum.CRYPTIC.name()))
                answerType.setImageResource(ResultEnum.CRYPTIC.getImageResource());
            else if (grade.equals(ResultEnum.WRONG.name()))
                answerType.setImageResource(ResultEnum.WRONG.getImageResource());
            else answerType.setImageResource(ResultEnum.PENDING.getImageResource());
        }

    }
}
