package ee.ut.sepp.natureapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.models.MyClass;
import ee.ut.sepp.natureapp.viewholders.MyClassViewHolder;

public class MyClassAdapter extends RecyclerView.Adapter<MyClassViewHolder> {

    private List<MyClass> classList;
    private Context context;

    public MyClassAdapter(Context context, List<MyClass> classList) {
        this.classList = classList;
        this.context = context;
    }

    @Override
    public MyClassViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View classView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_class_row, parent, false);

        return new MyClassViewHolder(context, classView);
    }

    @Override
    public void onBindViewHolder(MyClassViewHolder holder, int position) {
        MyClass myClass = classList.get(position);
        holder.onBind(myClass);
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }
}
