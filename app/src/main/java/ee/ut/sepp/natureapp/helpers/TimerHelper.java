package ee.ut.sepp.natureapp.helpers;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.widget.TextView;
import android.widget.Toast;

import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.models.Answer;

public class TimerHelper {
    private static CountDownTimer countDownTimer;
    private static long leftTime;
    private static boolean isFinished = false;

    public static void startGame(final Context context, final TextView tvLeftTime, long time) {

        countDownTimer = new CountDownTimer(time, 1000) {

           public void onTick(long millisUntilFinished) {
               tvLeftTime.setText(TimeHelper.getFormattedTime(millisUntilFinished));
               leftTime = millisUntilFinished;
           }

           public void onFinish() {
               isFinished = true;
               tvLeftTime.setText(context.getString(R.string.end_game));
               Answer answer = ModelFactory.createAnswer(context, SharedPrefencesHelper.readCurrentClassId((Activity) context),
                       SharedPrefencesHelper.readCurrentGroupId((Activity) context));
               FirebaseHelper.saveResults(context, answer, SharedPrefencesHelper.readCurrentClassId((Activity) context));
               Toast.makeText(context, context.getString(R.string.time_run_out), Toast.LENGTH_SHORT).show();
           }
       }.start();
   }

    public static long stopGame() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        return isFinished ? 0 : leftTime;
    }
}
