package ee.ut.sepp.natureapp.constants;


public class GeneralConstants {
    public static final String STORAGE_PATH_GROUP_PHOTO =  "/groups/";
    public static final String TASK_TOPIC_ID =  "taskTopicId";
    public static final String TASK_ID = "taskId";
    public static final String GROUP_ID = "groupId";
    public static final String CLASS_TIME_LIMIT = "classTimeLimit";
    public static final String CLASS_ID = "classId";
    public static final String CLASS_NAME = "className";
    public static final String LEFT_TIME = "leftTime";
    public static final String IS_GROUP = "isGroup";
    public static final String TASK_SUBMITTED_PHOTO = "submittedPhoto";
}
