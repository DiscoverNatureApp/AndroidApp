package ee.ut.sepp.natureapp.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import ee.ut.sepp.natureapp.R;

import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_TIME_LIMIT;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.GROUP_ID;

public class SharedPrefencesHelper {
    public static void writeCurrentClassId(Activity activity, String classId) {
        SharedPreferences prefs = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CLASS_ID, classId);
        editor.apply();
    }

    public static String readCurrentClassId(Activity activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE);
        return sharedPref.getString(CLASS_ID, "");
    }

    public static void writeCurrentGroupId(Activity activity, String groupId) {
        SharedPreferences prefs = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(GROUP_ID, groupId);
        editor.apply();
    }

    public static String readCurrentGroupId(Activity activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE);
        return sharedPref.getString(GROUP_ID, "");
    }


    public static void writeCurrentClassTimeLimit(Activity activity, int timeLimit) {
        SharedPreferences sp = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(CLASS_TIME_LIMIT, timeLimit);
        editor.commit();
    }

    public static int readCurrentClassTimeLimit(Activity activity) {
        SharedPreferences sp = activity.getSharedPreferences(activity.getString(R.string.app_name), Activity.MODE_PRIVATE);
        return sp.getInt(CLASS_TIME_LIMIT, 0);
    }
}
