package ee.ut.sepp.natureapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import ee.ut.sepp.natureapp.models.Group;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface GroupDao {
    @Query("SELECT * FROM groups WHERE classId = :classId")
    List<Group> getAllGroupsInClass(String classId);

    @Query("SELECT * FROM groups WHERE id = :groupId")
    Group getGroupById(String groupId);

    @Insert(onConflict = REPLACE)
    void insertNewGroup(Group group);

}
