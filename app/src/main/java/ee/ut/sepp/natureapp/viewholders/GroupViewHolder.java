package ee.ut.sepp.natureapp.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.helpers.ImageHelper;
import ee.ut.sepp.natureapp.models.Group;

public class GroupViewHolder extends RecyclerView.ViewHolder{
    @BindView(R.id.group_name)
    TextView groupName;
    @BindView(R.id.group_image)
    ImageView groupImage;
    @BindView(R.id.group_points)
    TextView groupPoints;

    private Context context;

    public GroupViewHolder(Context context, View itemView){
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void onBind(Group group){
        groupName.setText(group.getName());
        Glide.with(context).load(ImageHelper.decodeBitmap(group.getPhoto())).into(groupImage);
        groupPoints.setText(String.valueOf(AppDatabase.getAppDatabase(context).resultDao().getTotalScoreOfGroup(group.getId())));
    }
}
