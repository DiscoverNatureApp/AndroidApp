package ee.ut.sepp.natureapp.helpers;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageHelper {
    public static final String TAG = ImageHelper.class.getSimpleName();


    // this will be removed when we get data on web
    public static Bitmap drawableToBitmap(Context context, int resource) {
        return BitmapFactory.decodeResource(context.getResources(),
                resource);
    }

    /**
     * Encodes the given bitmap to String
     * @param bitmap
     * @return
     */
    public static String encodeBitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        String imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);

        return imageEncoded;
    }

    /**
     * Decodes the given String to the bitmap
     * @param encodedImage
     * @return
     */
    public static Bitmap decodeBitmap(String encodedImage) {
        Bitmap imageDecoded = null;
        if (encodedImage != null && !encodedImage.isEmpty()) {
            byte[] decodedBytes = Base64.decode(encodedImage, Base64.DEFAULT);
            imageDecoded = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        }

        return imageDecoded;
    }


    /**
     * If the storage is available, it saves the image into the device storage
     *
     * @param finalBitmap
     * @param path
     * @param fileName
     */
    public static void storeImageToDevice(Bitmap finalBitmap, String path, String fileName) {
        if (checkExternalStorage() && finalBitmap != null) {

            String storagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + path;
            File sdIconStorageDir = new File(storagePath);

            sdIconStorageDir.mkdirs();

            try {
                String filePath = sdIconStorageDir.toString() + "/" + fileName;
                FileOutputStream fileOutputStream = new FileOutputStream(filePath);

                BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

                finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);

                bos.flush();
                bos.close();
                Log.d(TAG, "File at path " + filePath + " was saved");

            } catch (FileNotFoundException e) {
                Log.w(TAG, "Error saving image file: " + e.getMessage());
            } catch (IOException e) {
                Log.w(TAG, "Error saving image file: " + e.getMessage());
            }

        }


    }


    /**
     * Checks if the external storage is available
     *
     * @return true if external storage is available, otherwise false
     */
    public static boolean checkExternalStorage() {
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }
}
