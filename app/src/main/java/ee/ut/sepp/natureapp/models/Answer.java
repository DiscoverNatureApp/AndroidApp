package ee.ut.sepp.natureapp.models;

import java.util.Map;

public class Answer {
    private String classId;
    private String groupId;
    private boolean gradingFinished;
    private int totalScore, noOfTasksToGrade;
    // ID_task, photo
    private Map<String, String> results;

    public Answer() {
    }

    public Answer(String classId, String groupId, boolean gradingFinished, int totalScore, int noOfTasksToGrade, Map<String, String> results) {
        this.classId = classId;
        this.groupId = groupId;
        this.gradingFinished = gradingFinished;
        this.totalScore = totalScore;
        this.noOfTasksToGrade = noOfTasksToGrade;
        this.results = results;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public int getNoOfTasksToGrade() {
        return noOfTasksToGrade;
    }

    public void setNoOfTasksToGrade(int noOfTasksToGrade) {
        this.noOfTasksToGrade = noOfTasksToGrade;
    }

    public boolean isGradingFinished() {
        return gradingFinished;
    }

    public void setGradingFinished(boolean gradingFinished) {
        this.gradingFinished = gradingFinished;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Map<String, String> getResults() {
        return results;
    }

    public void setResults(Map<String, String> results) {
        this.results = results;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "groupId='" + groupId + '\'' +
                ", gradingFinished=" + gradingFinished +
                ", results=" + results +
                '}';
    }
}
