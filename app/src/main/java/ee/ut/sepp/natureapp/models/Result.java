package ee.ut.sepp.natureapp.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "results")
public class Result {
    @PrimaryKey
    @NonNull
    private String picture;

    private String taskId;
    private String groupId;
    private int totalScore;
    private boolean gradingFinished;
    private String grade;

    @Ignore
    public Result(){}

    public Result(@NonNull String picture, String taskId, String groupId, int totalScore, boolean gradingFinished, String grade) {
        this.picture = picture;
        this.taskId = taskId;
        this.groupId = groupId;
        this.totalScore = totalScore;
        this.gradingFinished = gradingFinished;
        this.grade = grade;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    @NonNull
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(@NonNull String groupId) {
        this.groupId = groupId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Result{" +
                "picture='" + picture + '\'' +
                ", taskId='" + taskId + '\'' +
                ", groupId='" + groupId + '\'' +
                ", totalScore=" + totalScore +
                '}';
    }

    public boolean isGradingFinished() {
        return gradingFinished;
    }

    public void setGradingFinished(boolean gradingFinished) {
        this.gradingFinished = gradingFinished;
    }
}
