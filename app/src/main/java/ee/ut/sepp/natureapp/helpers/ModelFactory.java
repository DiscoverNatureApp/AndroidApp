package ee.ut.sepp.natureapp.helpers;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.models.Answer;
import ee.ut.sepp.natureapp.models.Group;
import ee.ut.sepp.natureapp.models.Task;


public class ModelFactory {

    /**
     * Creates a new instance Group
     *
     * @param id
     * @param groupNameLayout
     * @param photo
     * @param memberLayouts
     * @return Group
     */
    public static Group createGroup(String id, String classId, TextInputLayout groupNameLayout, String photo, List<TextInputLayout> memberLayouts) {

        String groupName = ModelHelper.getProperty(groupNameLayout);
        ArrayList<String> members = new ArrayList<>();
        for (TextInputLayout textInputLayout : memberLayouts) {
            members.add(ModelHelper.getProperty(textInputLayout));
        }
        return new Group(id, classId, groupName, photo, members);
    }

    public static Answer createAnswer(Context context, String classId, String groupId) {
        AppDatabase db = AppDatabase.getAppDatabase(context);
        Map<String, String> resultMap = new HashMap<>();
        // results all tasks not depending on topics
        for (Task task : db.taskDao().getAllTasksInClass(classId)) {
            if (task.getSubmittedPhoto() != null)
                resultMap.put(task.getId(), task.getSubmittedPhoto());
        }

        return new Answer(classId, groupId, false, 0, resultMap.size(), resultMap);
    }


    private static class ModelHelper {
        /**
         * Gets specified property of specified object
         *
         * @param inputTextLayout
         * @return
         */
        public static String getProperty(Object inputTextLayout) {
            String property = "";
            if (inputTextLayout instanceof TextInputLayout) {
                EditText editText = ((TextInputLayout) inputTextLayout).getEditText();
                property = editText.getText().toString();
            }
            return property;
        }
    }
}
