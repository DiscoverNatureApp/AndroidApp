package ee.ut.sepp.natureapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import ee.ut.sepp.natureapp.models.Result;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ResultDao {
    @Query("SELECT * FROM results WHERE groupId = :groupId")
    List<Result> getResultsByGroup(String groupId);

    @Query("SELECT * FROM results WHERE groupId = :groupId AND taskId = :taskId")
    Result getResultOfTaskByGroup(String groupId, String taskId);

    @Query("SELECT gradingFinished FROM results WHERE groupId = :groupId AND taskId = :taskId")
    boolean gradingFinishedOfTask(String groupId, String taskId);

    @Query("SELECT grade FROM results WHERE groupId = :groupId AND taskId = :taskId")
    String getGradeOfTask(String groupId, String taskId);

    @Query("SELECT totalScore FROM results WHERE groupId = :groupId")
    int getTotalScoreOfGroup(String groupId);

    @Insert(onConflict = REPLACE)
    void insertNewResult(Result result);


}
