package ee.ut.sepp.natureapp.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import ee.ut.sepp.natureapp.dao.GroupDao;
import ee.ut.sepp.natureapp.dao.MyClassDao;
import ee.ut.sepp.natureapp.dao.ResultDao;
import ee.ut.sepp.natureapp.dao.TaskDao;
import ee.ut.sepp.natureapp.dao.TopicDao;
import ee.ut.sepp.natureapp.models.Group;
import ee.ut.sepp.natureapp.models.MyClass;
import ee.ut.sepp.natureapp.models.Result;
import ee.ut.sepp.natureapp.models.Task;
import ee.ut.sepp.natureapp.models.Topic;


@Database(entities = {MyClass.class, Task.class, Topic.class, Group.class, Result.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract MyClassDao myClassDao();
    public abstract GroupDao groupDao();
    public abstract TaskDao taskDao();
    public abstract TopicDao topicDao();
    public abstract ResultDao resultDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "nature-database")
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
