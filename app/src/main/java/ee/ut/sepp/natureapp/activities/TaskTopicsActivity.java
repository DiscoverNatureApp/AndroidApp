package ee.ut.sepp.natureapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.adapters.TaskTopicAdapter;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.helpers.FirebaseHelper;
import ee.ut.sepp.natureapp.helpers.ModelFactory;
import ee.ut.sepp.natureapp.helpers.SharedPrefencesHelper;
import ee.ut.sepp.natureapp.helpers.TimerHelper;
import ee.ut.sepp.natureapp.listeners.RecyclerTouchListener;
import ee.ut.sepp.natureapp.models.Answer;
import ee.ut.sepp.natureapp.models.Topic;

import static ee.ut.sepp.natureapp.constants.GeneralConstants.LEFT_TIME;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.TASK_TOPIC_ID;

// can be added later if on the web will form for creating taskTopics
public class TaskTopicsActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.left_time)
    TextView leftTimeTv;

    private List<Topic> topicList;
    private TaskTopicAdapter topicAdapter;
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_topics);
        ButterKnife.bind(this);

        db = AppDatabase.getAppDatabase(this);

        setupRecyclerView();

        if (getIntent().hasExtra(LEFT_TIME)) {
            TimerHelper.startGame(this, leftTimeTv, getIntent().getLongExtra(LEFT_TIME, 0));
        } else {
            TimerHelper.startGame(this, leftTimeTv, SharedPrefencesHelper.readCurrentClassTimeLimit(TaskTopicsActivity.this) * 60000);
        }
    }

    private void setupRecyclerView() {
        topicList = new ArrayList<>();
        topicAdapter = new TaskTopicAdapter(this, topicList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(topicAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(TaskTopicsActivity.this, TasksActivity.class);
                intent.putExtra(TASK_TOPIC_ID, topicList.get(position).getId());
                intent.putExtra(LEFT_TIME, TimerHelper.stopGame());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }

        }));

        //prepareTopics();
        topicList.addAll(db.topicDao().getAllTopic());
    }

    // tasktopic screen can be used
    /*private void prepareTopics() {
        db.topicDao().insertNewTopic(new Topic("1", "Trees", TopicStatus.DONE.name(), ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(this, R.drawable.tree))));
        db.topicDao().insertNewTopic(new Topic("2", "Animal", TopicStatus.IN_PROGRESS.name(), ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(this, R.drawable.animal))));
        db.topicDao().insertNewTopic(new Topic("3", "Insect", TopicStatus.DONE.name(), ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(this, R.drawable.insect))));
        db.topicDao().insertNewTopic(new Topic("4", "Flower", TopicStatus.IN_PROGRESS.name(), ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(this, R.drawable.flower))));
    }*/

    @OnClick(R.id.btn_submit_results)
    void submit(){
        FirebaseHelper.saveResults(this, prepareAnswer(), SharedPrefencesHelper.readCurrentClassId(this));
        Toast.makeText(TaskTopicsActivity.this, getString(R.string.answers_saved), Toast.LENGTH_SHORT).show();
        TimerHelper.stopGame();
        Intent intent = new Intent(TaskTopicsActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private Answer prepareAnswer() {
        return ModelFactory.createAnswer(this, SharedPrefencesHelper.readCurrentClassId(this),
                SharedPrefencesHelper.readCurrentGroupId(TaskTopicsActivity.this));
    }

}
