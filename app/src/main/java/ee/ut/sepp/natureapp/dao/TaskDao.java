package ee.ut.sepp.natureapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import ee.ut.sepp.natureapp.models.Task;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface TaskDao {
    @Query("SELECT * FROM tasks ORDER BY title ASC")
    List<Task> getAllTasks();

    @Query("SELECT * FROM tasks WHERE classId = :classId ORDER BY title ASC")
    List<Task> getAllTasksInClass(String classId);

    @Query("SELECT * FROM tasks WHERE id = :taskId")
    Task getTaskById(String taskId);

    @Insert(onConflict = REPLACE)
    void insertNewTask(Task task);

    @Query("UPDATE tasks SET submittedPhoto = :submittedPhoto, status = :status  WHERE id = :taskId")
    void submitTakenPhoto(String taskId, String submittedPhoto, String status);
}
