package ee.ut.sepp.natureapp.helpers;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.models.Answer;
import ee.ut.sepp.natureapp.models.Group;
import ee.ut.sepp.natureapp.models.Task;

import static ee.ut.sepp.natureapp.constants.FirebaseConstants.ANSWER_NODE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_NODE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.TASKS_TO_GRADE_NODE;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.GROUP_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.STORAGE_PATH_GROUP_PHOTO;

/**
 * This class covers work with Firebase
 */

public class FirebaseHelper {
    private static DatabaseReference databaseReference;

    public static DatabaseReference getDatabaseReference() {
        if (databaseReference == null) {
            databaseReference = FirebaseDatabase.getInstance().getReference();
        }

        return databaseReference;
    }

    /**
     * Saves a new group to Firebase database and saves the group photo into external storage
     * @param context
     * @param newGroup
     */
    public static void saveNewGroup(Context context, Group newGroup) {
        String id = getDatabaseReference().child(GROUP_NODE).push().getKey();
        newGroup.setId(id);
        // save current groupId
        SharedPrefencesHelper.writeCurrentGroupId((Activity) context, id);

        if (newGroup.getPhoto() !=  null) {
            ImageHelper.storeImageToDevice(ImageHelper.decodeBitmap(newGroup.getPhoto()),
                    context.getPackageName() + STORAGE_PATH_GROUP_PHOTO, newGroup.getId() + ".jpg");

        }

        getDatabaseReference().child(GROUP_NODE).child(id).setValue(newGroup);
    }

    public static void saveResults(Context context, Answer answer, String classId) {
        getDatabaseReference().child(ANSWER_NODE).child(answer.getGroupId()).setValue(answer);
        for (Task task: AppDatabase.getAppDatabase(context).taskDao().getAllTasksInClass(classId)) {
            if (task.getSubmittedPhoto() != null) {
                HashMap<String, String> groupHashMap = new HashMap<>();
                groupHashMap.put(GROUP_ID, answer.getGroupId());
                getDatabaseReference().child(TASKS_TO_GRADE_NODE).child(task.getId()).push().setValue(groupHashMap);
            }

        }
    }


}
