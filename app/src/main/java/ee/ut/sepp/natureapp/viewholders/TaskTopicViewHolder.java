package ee.ut.sepp.natureapp.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.enums.TopicStatus;
import ee.ut.sepp.natureapp.helpers.ImageHelper;
import ee.ut.sepp.natureapp.models.Topic;

public class TaskTopicViewHolder extends RecyclerView.ViewHolder {
    Context context;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.status_iv)
    ImageView status;

    public TaskTopicViewHolder(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void onBind(Topic topic) {
        if (topic.getStatus().equals(TopicStatus.DONE.name())) {
            status.setImageResource(TopicStatus.DONE.getImageResource());
        } else if (topic.getStatus().equals(TopicStatus.IN_PROGRESS.name())) {
            status.setImageResource(TopicStatus.IN_PROGRESS.getImageResource());
        }
        name.setText(topic.getName());
        Glide.with(context).load(ImageHelper.decodeBitmap(topic.getImage())).into(image);
    }
}
