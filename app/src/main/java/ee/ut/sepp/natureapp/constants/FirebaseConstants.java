package ee.ut.sepp.natureapp.constants;


public class FirebaseConstants {
    // TODO change to class in web
    public static final String CLASS_NODE = "classes";
    public static final String PASSWORD_FIELD= "passw";
    public static final String GROUP_NODE = "groups";
    public static final String GROUP_CLASS_ID = "classId";
    public static final String GROUP_UNIQUE_ID = "id";
    public static final String GROUP_NAME = "name";
    public static final String GROUP_PHOTO = "photo";
    public static final String GROUP_MEMBERS = "members";
    public static final String ANSWER_NODE = "answers";
    public static final String TASKS_TO_GRADE_NODE = "tasksToGrade";
    public static final String ANSWER_RESULTS = "results";
    public static final String ANSWER_TOTAL_SCORE = "totalScore";
    public static final String ANSWER_GRADING_FINISHED = "gradingFinished";
    public static final String ANSWER_GRADE = "grades";
    public static final String TASK_NODE = "tasks";
    public static final String TASK_IN_CLASS_NODE = "tasksInClass";
    public static final String TASK_ID = "id";
    public static final String TASK_TITLE = "name";
    public static final String TASK_DESC = "description";
    public static final String TASK_PICTURE = "picture";
}
