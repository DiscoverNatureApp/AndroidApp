package ee.ut.sepp.natureapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.adapters.GroupsAdapter;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.listeners.RecyclerTouchListener;
import ee.ut.sepp.natureapp.models.Group;

import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_NAME;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.GROUP_ID;

public class ViewGroupsActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    @BindView(R.id.groups_view)
    RecyclerView groupsView;

    @BindView(R.id.tv_no_groups)
    TextView noGroupsTv;

    private List<Group> groupList = new ArrayList<>();
    private GroupsAdapter groupsAdapter;
    AppDatabase db;
    String classId, className;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_groups);
        ButterKnife.bind(this);

        if (getIntent().hasExtra(CLASS_ID) && getIntent().hasExtra(CLASS_NAME)) {
            classId = getIntent().getStringExtra(CLASS_ID);
            className = getIntent().getStringExtra(CLASS_NAME);
        }

        if (className == null) className = "class";
        setTitle(getString(R.string.groups_in_class) + " " + className);

        db = AppDatabase.getAppDatabase(this);

        setupRecyclerView();

    }


    @OnClick(R.id.btn_create_group)
    void createGroup(){
        Intent intent = new Intent(ViewGroupsActivity.this, NewGroupActivity.class);
        intent.putExtra(CLASS_ID, classId);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_items, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        List<Group> newList = new ArrayList<>();

        for (Group group : groupList){
            String name = group.getName().toLowerCase();
            if (name.contains(newText)){
                newList.add(group);
            }
        }

        groupsAdapter.setFilter(newList);
        if (newList.size() == 0) {
            noGroupsTv.setVisibility(View.VISIBLE);
        } else {
            noGroupsTv.setVisibility(View.GONE);
        }

        return true;
    }

    private void setupRecyclerView() {
        groupsView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), groupsView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position){
                Group group = (Group) groupsAdapter.getItem(position);
                Intent intent = new Intent(ViewGroupsActivity.this, GroupsDetailActivity.class);
                intent.putExtra(GROUP_ID, group.getId());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position){}

        }));


        groupsAdapter = new GroupsAdapter(this, groupList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        groupsView.setLayoutManager(mLayoutManager);
        groupsView.setItemAnimator(new DefaultItemAnimator());
        groupsView.setAdapter(groupsAdapter);

        groupList.addAll(db.groupDao().getAllGroupsInClass(classId));
        if (groupList.size() == 0)
            noGroupsTv.setVisibility(View.VISIBLE);
        else noGroupsTv.setVisibility(View.GONE);

        groupsAdapter.notifyDataSetChanged();

    }


}
