package ee.ut.sepp.natureapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.helpers.FirebaseHelper;
import ee.ut.sepp.natureapp.helpers.FontHelper;
import ee.ut.sepp.natureapp.helpers.SharedPrefencesHelper;
import ee.ut.sepp.natureapp.models.MyClass;

import static ee.ut.sepp.natureapp.constants.FirebaseConstants.CLASS_NODE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.PASSWORD_FIELD;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_NAME;
import static ee.ut.sepp.natureapp.constants.SettingsConstants.CUSTOM_FONT;

public class JoinNewClassActivity extends AppCompatActivity {

    public static final String TAG = JoinNewClassActivity.class.getSimpleName();

    @BindView(R.id.class_code_layout)
    TextInputLayout classCodeLayout;
    @BindView(R.id.class_code)
    TextInputEditText classCode;
    @BindView(R.id.class_title)
    TextView classTitle;

    private AppDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_new_class);
        ButterKnife.bind(this);
        classCodeLayout.setErrorEnabled(true);
        FontHelper.setFont(this, classTitle, CUSTOM_FONT);
        db = AppDatabase.getAppDatabase(this);
    }

    @OnClick(R.id.btn_join_class)
    void joinNewClass() {
        DatabaseReference reference = FirebaseHelper.getDatabaseReference();

        Query query = reference.child(CLASS_NODE).orderByChild(PASSWORD_FIELD).equalTo(classCode.getText().toString());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Toast.makeText(JoinNewClassActivity.this, getString(R.string.confirmation), Toast.LENGTH_SHORT).show();

                    String[] pieces = ((HashMap) dataSnapshot.getValue()).values().toArray()[0].toString().split(", ");
                    String classId = (String) ((HashMap) dataSnapshot.getValue()).keySet().toArray()[0];
                    String className = pieces[1].split("=")[1];
                    int timeLimit = Integer.parseInt(pieces[3].split("=")[1]);
                    SharedPrefencesHelper.writeCurrentClassTimeLimit(JoinNewClassActivity.this, timeLimit);
                    SharedPrefencesHelper.writeCurrentClassId(JoinNewClassActivity.this, classId);

                    db.myClassDao().insertNewClass(new MyClass(classId, className));
                    Intent intent = new Intent(JoinNewClassActivity.this, LoadingScreenActivity.class);
                    intent.putExtra(CLASS_ID, classId);
                    intent.putExtra(CLASS_NAME, className);
                    startActivity(intent);
                    Log.d(TAG, "The class exists");
                }
                else {
                    classCodeLayout.setError(getString(R.string.wrong_group_code));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "The class doesn't exist");
            }
        });
    }

}
