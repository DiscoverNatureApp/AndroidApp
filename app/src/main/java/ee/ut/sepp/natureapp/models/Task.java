package ee.ut.sepp.natureapp.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "tasks", foreignKeys = @ForeignKey(entity = MyClass.class,
        parentColumns = "id",childColumns = "classId", onDelete = CASCADE))
public class Task{
    @PrimaryKey()
    @NonNull
    private String id;
    private String classId, title, subtitle, imageDesc, description, submittedPhoto, status;

    @Ignore
    public Task(){
    }

    @Ignore
    public Task(String title, String subtitle, String description, String imageDesc){
        this.title = title;
        this.subtitle = subtitle;
        this.imageDesc = imageDesc;
        this.description = description;
    }

    public Task(String id, String classId, String title, String subtitle, String description, String imageDesc){
        this.id = id;
        this.classId = classId;
        this.title = title;
        this.subtitle = subtitle;
        this.imageDesc = imageDesc;
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubmittedPhoto() {
        return submittedPhoto;
    }

    public void setSubmittedPhoto(String submittedPhoto) {
        this.submittedPhoto = submittedPhoto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getSubtitle(){
        return subtitle;
    }

    public void setSubtitle(String subtitle){
        this.subtitle = subtitle;
    }

    public String getImageDesc(){
        return imageDesc;
    }

    public void setImageDesc(String imageDesc){
        this.imageDesc = imageDesc;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", classId='" + classId + '\'' +
                ", title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", imageDesc='" + imageDesc + '\'' +
                ", description='" + description + '\'' +
                ", submittedPhoto='" + submittedPhoto + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
