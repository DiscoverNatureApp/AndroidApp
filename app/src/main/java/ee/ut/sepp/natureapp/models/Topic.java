package ee.ut.sepp.natureapp.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "topics")
public class Topic {
    @PrimaryKey
    @NonNull
    private String id;
    private String name, status, image;

    @Ignore
    public Topic() {
    }

    @Ignore
    public Topic(String name, String status, String image) {
        this.status = status;
        this.name = name;
        this.image = image;
    }

    public Topic(String id, String name, String status, String image) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", image='" + image + '\'' +
                '}';
    }


}
