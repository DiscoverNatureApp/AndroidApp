package ee.ut.sepp.natureapp.helpers;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

import ee.ut.sepp.natureapp.R;

public class DialogHelper {
    static ProgressDialog progressDialog;

    public static void showFullSizeImage(Context context, Bitmap photo) {
        final Dialog nagDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.preview_image);
        ImageButton btnClose = (ImageButton) nagDialog.findViewById(R.id.btn_close);
        ImageView ivPreview = (ImageView) nagDialog.findViewById(R.id.iv_preview_image);
        ivPreview.setImageBitmap(photo);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();

    }

    public static void showTakenPhotoOptionsDialog(Context context, DialogInterface.OnClickListener viewPictureListener, DialogInterface.OnClickListener takePictureListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(context.getString(R.string.photo_options))
                .setMessage(context.getString(R.string.photo_options_message))
                .setCancelable(true)
                .setPositiveButton(context.getString(R.string.take_picture_again), takePictureListener)
                .setNegativeButton(context.getString(R.string.view_taken_picture), viewPictureListener);
        builder.show();

    }

    public static void showProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(context.getString(R.string.checking_group_name));
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public static void dismissProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

}
