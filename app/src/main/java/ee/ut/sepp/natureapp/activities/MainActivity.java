package ee.ut.sepp.natureapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.adapters.MyClassAdapter;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.helpers.SharedPrefencesHelper;
import ee.ut.sepp.natureapp.listeners.RecyclerTouchListener;
import ee.ut.sepp.natureapp.models.MyClass;

import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_NAME;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private MyClassAdapter myClassAdapter;
    private List<MyClass> myClassList = new ArrayList<>();
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        db = AppDatabase.getAppDatabase(this);
        myClassList.addAll(db.myClassDao().getAllMyClasses());

        setupRecyclerView();

    }

    @OnClick(R.id.btn_show_joining_form)
    void showJoiningClassForm() {
        Intent intent = new Intent(MainActivity.this, JoinNewClassActivity.class);
        startActivity(intent);
    }

    private void setupRecyclerView() {
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position){
                MyClass myClass = myClassList.get(position);
                Intent intent = new Intent(MainActivity.this, LoadingScreenActivity.class);
                intent.putExtra(CLASS_ID, myClass.getId());
                intent.putExtra(CLASS_NAME, myClass.getName());
                SharedPrefencesHelper.writeCurrentClassId(MainActivity.this, myClass.getId());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position){}

        }));

        myClassAdapter = new MyClassAdapter(this, myClassList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(myClassAdapter);

//        myClassAdapter.notifyDataSetChanged();

    }
}
