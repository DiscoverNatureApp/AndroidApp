package ee.ut.sepp.natureapp.enums;

import ee.ut.sepp.natureapp.R;

/**
 * Created by admin on 10.12.2017.
 */

public enum ResultEnum {
    PENDING(R.drawable.pending_to_grade),
    CORRECT(R.drawable.checked),
    WRONG(R.drawable.wrong_answer),
    CRYPTIC(R.drawable.cryptic);

    private int imageResource;
    ResultEnum(int imageResource) {
        this.imageResource = imageResource;
    }

    public int getImageResource() {
        return imageResource;
    }
}
