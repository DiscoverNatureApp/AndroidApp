package ee.ut.sepp.natureapp.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.models.MyClass;


public class MyClassViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.class_name)
    TextView name;
    @BindView(R.id.class_image)
    ImageView image;

    private Context context;

    public MyClassViewHolder(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void onBind(MyClass myClass) {
        name.setText(myClass.getName());
        image.setImageResource(R.drawable.class_logo);
    }
}
