package ee.ut.sepp.natureapp.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "groups", foreignKeys = @ForeignKey(entity = MyClass.class,
        parentColumns = "id",
        childColumns = "classId",
        onDelete = CASCADE))
public class Group {
    @PrimaryKey
    @NonNull
    private String id;

    private String classId;
    private String name, photo;
    private ArrayList<String> members;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public ArrayList<String> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<String> members) {
        this.members = members;
    }

    @Ignore
    public Group() {
    }

    public Group(String id, String classId, String name, String photo, ArrayList<String> members) {
        this.classId = classId;
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.members = members;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id='" + id + '\'' +
                ", classId='" + classId + '\'' +
                ", name='" + name + '\'' +
                ", photo='" + photo + '\'' +
                ", members=" + members +
                '}';
    }
}
