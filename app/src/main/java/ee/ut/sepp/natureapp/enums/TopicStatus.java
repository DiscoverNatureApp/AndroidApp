package ee.ut.sepp.natureapp.enums;

import ee.ut.sepp.natureapp.R;

public enum TopicStatus {
        IN_PROGRESS(R.drawable.hourglass),
        DONE(R.drawable.checked);

        private int imageResource;
        TopicStatus(int imageResource) {
            this.imageResource = imageResource;
        }

        public int getImageResource() {
            return imageResource;
        }
}
