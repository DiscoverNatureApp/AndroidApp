package ee.ut.sepp.natureapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.helpers.SharedPrefencesHelper;
import ee.ut.sepp.natureapp.helpers.TimeHelper;

public class StartGameActivity extends AppCompatActivity {
    @BindView(R.id.left_time)
    TextView time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);
        ButterKnife.bind(this);
        time.setText(TimeHelper.getFormattedTime(SharedPrefencesHelper.readCurrentClassTimeLimit(StartGameActivity.this) * 60000));
    }

    @OnClick(R.id.btn_show_start)
    void startGame(){
        Intent intent = new Intent(StartGameActivity.this, TasksActivity.class);
        startActivity(intent);
    }

}
