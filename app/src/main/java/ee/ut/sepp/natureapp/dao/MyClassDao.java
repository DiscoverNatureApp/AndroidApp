package ee.ut.sepp.natureapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import ee.ut.sepp.natureapp.models.MyClass;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface MyClassDao {
    @Query("SELECT * FROM myClasses ORDER BY name ASC")
    List<MyClass> getAllMyClasses();

    @Query("SELECT * FROM myClasses WHERE id = :classId")
    MyClass getMyClassById(String classId);

    @Insert(onConflict = REPLACE)
    void insertNewClass(MyClass myClass);


}
