package ee.ut.sepp.natureapp.constants;

import android.Manifest;

public class PermissionsConstants {
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int PERMISSIONS_GROUP = 2;
    public static  final String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

}
