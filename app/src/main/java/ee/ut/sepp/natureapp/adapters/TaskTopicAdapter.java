package ee.ut.sepp.natureapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.models.Topic;
import ee.ut.sepp.natureapp.viewholders.TaskTopicViewHolder;

public class TaskTopicAdapter extends RecyclerView.Adapter<TaskTopicViewHolder> {

    private Context context;
    private List<Topic> topiclist;

    public TaskTopicAdapter(Context context, List<Topic> topiclist) {
        this.context = context;
        this.topiclist = topiclist;
    }

    @Override
    public TaskTopicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_topic_card, parent, false);

        return new TaskTopicViewHolder(context, itemView);
    }

    @Override
    public void onBindViewHolder(TaskTopicViewHolder holder, int position) {
        Topic topic = topiclist.get(position);
        holder.onBind(topic);
    }


    @Override
    public int getItemCount() {
        return topiclist.size();
    }
}