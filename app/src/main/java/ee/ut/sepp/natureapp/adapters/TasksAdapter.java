package ee.ut.sepp.natureapp.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import ee.ut.sepp.natureapp.models.Task;
import ee.ut.sepp.natureapp.viewholders.TaskViewHolder;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class TasksAdapter extends RecyclerView.Adapter<TaskViewHolder> implements Filterable {

    private static final String TAG = TasksAdapter.class.getSimpleName();
    private final int layout;
    private View emptyView;
    private List<Task> tasksList = new ArrayList<>();
    private Context context;
    // for filtering
    private List<Task> filterTasks = new ArrayList<>();
    private String groupId;

    public TasksAdapter(Context context, List<Task> tasksList, View emptyView, int layout, String groupId) {
        this.tasksList = tasksList;
        this.context = context;
        this.filterTasks = tasksList;
        this.layout = layout;
        this.emptyView = emptyView;
        this.groupId = groupId;

    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View taskView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);

        return new TaskViewHolder(context, taskView, groupId);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Task task = filterTasks.get(position);
        holder.onBind(task);
    }

    @Override
    public int getItemCount(){
        return filterTasks.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.i(TAG, "Filter.performFiltering");
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    // return all list
                    results.values = tasksList;
                    results.count = tasksList.size();
                } else {
                    constraint = constraint.toString().toLowerCase();

                    List<Task> found = new ArrayList<>();
                    for (Task item : tasksList) {
                        if (item.getTitle().toLowerCase().contains(constraint)) {
                            found.add(item);
                        }

                        results.values = found;
                        results.count = found.size();
                    }

                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filterTasks = (List<Task>) results.values;
                if (filterTasks.size() == 0) emptyView.setVisibility(VISIBLE);
                else emptyView.setVisibility(GONE);
                notifyDataSetChanged();
            }
        };
    }


}
