package ee.ut.sepp.natureapp.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.enums.ResultEnum;
import ee.ut.sepp.natureapp.enums.TaskStatus;
import ee.ut.sepp.natureapp.helpers.ImageHelper;
import ee.ut.sepp.natureapp.models.Result;
import ee.ut.sepp.natureapp.models.Task;


public class TaskViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.submitted_photo)
    CircleImageView submittedPhoto;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.subtitle)
    TextView titleDesc;
    @BindView(R.id.status_icon)
    ImageView statusIcon;

    private Context context;
    private String groupId;

    public TaskViewHolder(Context context, View itemView, String groupId) {
        super(itemView);

        ButterKnife.bind(this, itemView);
        this.context = context;
        this.groupId = groupId;
    }

    public void onBind(Task task) {
        title.setText(task.getTitle());
        titleDesc.setText(task.getSubtitle());
        if (task.getImageDesc() != null) {
            Glide.with(context).load(ImageHelper.decodeBitmap(task.getImageDesc())).into(submittedPhoto);
        }
        if (task.getStatus() != null) {
            if (task.getStatus().equals(TaskStatus.DONE.name())) {
                statusIcon.setImageResource(TaskStatus.DONE.getImageResource());
                Glide.with(context).load(ImageHelper.decodeBitmap(task.getSubmittedPhoto())).into(submittedPhoto);
            } else if (task.getStatus().equals(TaskStatus.PENDING.name())) {
                statusIcon.setImageResource(TaskStatus.PENDING.getImageResource());

            }
        }

        if (groupId != null && !groupId.isEmpty()) {
            Result result = AppDatabase.getAppDatabase(context).resultDao().getResultOfTaskByGroup(groupId, task.getId());
            String grade = result.getGrade().toUpperCase();
            Glide.with(context).load(ImageHelper.decodeBitmap(result.getPicture())).into(submittedPhoto);

            if (grade.equals((ResultEnum.CORRECT.name()))) statusIcon.setImageResource(ResultEnum.CORRECT.getImageResource());
            else if (grade.equals(ResultEnum.CRYPTIC.name())) statusIcon.setImageResource(ResultEnum.CRYPTIC.getImageResource());
            else if (grade.equals(ResultEnum.WRONG.name())) statusIcon.setImageResource(ResultEnum.WRONG.getImageResource());
            else statusIcon.setImageResource(ResultEnum.PENDING.getImageResource());

        }
    }
}
