package ee.ut.sepp.natureapp.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.adapters.TasksAdapter;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.helpers.FirebaseHelper;
import ee.ut.sepp.natureapp.helpers.ModelFactory;
import ee.ut.sepp.natureapp.helpers.SharedPrefencesHelper;
import ee.ut.sepp.natureapp.helpers.TimerHelper;
import ee.ut.sepp.natureapp.listeners.RecyclerTouchListener;
import ee.ut.sepp.natureapp.models.Answer;
import ee.ut.sepp.natureapp.models.Result;
import ee.ut.sepp.natureapp.models.Task;

import static ee.ut.sepp.natureapp.constants.GeneralConstants.GROUP_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.IS_GROUP;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.LEFT_TIME;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.TASK_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.TASK_SUBMITTED_PHOTO;

public class TasksActivity extends AppCompatActivity {
    private static final String TAG = TasksActivity.class.getSimpleName();

    @BindView(R.id.btn_submit_results)
    Button submitBtn;
    @BindView(R.id.timer)
    RelativeLayout timerLayout;
    @BindView(R.id.tasks_view)
    RecyclerView tasksView;
    @BindView(R.id.left_time)
    TextView leftTimeTv;

    @BindView(R.id.tv_no_tasks) TextView noTasksTv;

    private List<Task> taskList = new ArrayList<>();
    Task task;
    private TasksAdapter tasksAdapter;
    private AppDatabase db;
    private boolean isGroup;
    private String groupId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        ButterKnife.bind(this);
        db = AppDatabase.getAppDatabase(this);

        if (getIntent().hasExtra(IS_GROUP)) {
            isGroup = getIntent().getBooleanExtra(IS_GROUP, false);
            groupId = getIntent().getStringExtra(GROUP_ID);

            List<Result> results = db.resultDao().getResultsByGroup(groupId);
            if (results.size() != 0) {
                for (Result result : results) {
                    task = db.taskDao().getTaskById(result.getTaskId());
                    task.setSubmittedPhoto(result.getPicture());
                    taskList.add(task);
                }
            }
            if (taskList.size() == 0) noTasksTv.setVisibility(View.VISIBLE);
            else noTasksTv.setVisibility(View.GONE);

            timerLayout.setVisibility(View.GONE);
            submitBtn.setVisibility(View.GONE);
        } else {
            taskList.addAll(db.taskDao().getAllTasksInClass(SharedPrefencesHelper.readCurrentClassId(this)));
            timerLayout.setVisibility(View.VISIBLE);
            submitBtn.setVisibility(View.VISIBLE);
        }

        if (getIntent().hasExtra(LEFT_TIME)) {
            TimerHelper.startGame(this, leftTimeTv, getIntent().getLongExtra(LEFT_TIME, 0));
        } else {
            TimerHelper.startGame(this, leftTimeTv, SharedPrefencesHelper.readCurrentClassTimeLimit(this) * 60000);
        }

        setupRecyclerView();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.tasks_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            final SearchView finalSearchView = searchView;
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    tasksAdapter.getFilter().filter(query);
                    if( !finalSearchView.isIconified()) {
                        finalSearchView.setIconified(true);
                    }
                    searchItem.collapseActionView();
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String s) {
                    tasksAdapter.getFilter().filter(s);
                    return false;
                }
            });

        }


        return super.onCreateOptionsMenu(menu);
    }


    @OnClick(R.id.btn_submit_results)
    void submitAnswers(){
        FirebaseHelper.saveResults(this, prepareAnswer(), SharedPrefencesHelper.readCurrentClassId(this));
        Toast.makeText(this, getString(R.string.answers_saved), Toast.LENGTH_SHORT).show();
        TimerHelper.stopGame();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private Answer prepareAnswer() {
        return ModelFactory.createAnswer(this, SharedPrefencesHelper.readCurrentClassId(this),
                SharedPrefencesHelper.readCurrentGroupId(this));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isGroup){
                    onBackPressed();
                }
//                else {
//                    Intent intent = new Intent(TasksActivity.this, TaskTopicsActivity.class);
//                    intent.putExtra(LEFT_TIME, TimerHelper.stopGame());
//                    startActivity(intent);
//                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupRecyclerView() {
        tasksView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), tasksView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position){
                Task task = taskList.get(position);
                Intent intent = new Intent(TasksActivity.this, TaskDetailActivity.class);
                intent.putExtra(TASK_ID, task.getId());
                if (isGroup) {
                    intent.putExtra(TASK_SUBMITTED_PHOTO, task.getSubmittedPhoto());
                    intent.putExtra(GROUP_ID, groupId);
                }
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position){}

        }));
        int layout = isGroup ? R.layout.group_task_row : R.layout.task_row;
        tasksAdapter = new TasksAdapter(this, taskList, noTasksTv, layout, groupId);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        tasksView.setLayoutManager(mLayoutManager);
        tasksView.setItemAnimator(new DefaultItemAnimator());
        tasksView.setAdapter(tasksAdapter);

        tasksAdapter.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed(){
        if (isGroup){
            finish();
            Intent intent = new Intent(TasksActivity.this, GroupsDetailActivity.class);
            intent.putExtra(GROUP_ID, groupId);
            startActivity(intent);
        }
        else super.onBackPressed();
    }
}
