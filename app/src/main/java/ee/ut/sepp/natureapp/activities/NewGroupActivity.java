package ee.ut.sepp.natureapp.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mvc.imagepicker.ImagePicker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.helpers.DialogHelper;
import ee.ut.sepp.natureapp.helpers.FirebaseHelper;
import ee.ut.sepp.natureapp.helpers.ImageHelper;
import ee.ut.sepp.natureapp.helpers.ModelFactory;
import ee.ut.sepp.natureapp.helpers.SharedPrefencesHelper;

import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_NAME;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_NODE;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_ID;
import static ee.ut.sepp.natureapp.constants.PermissionsConstants.PERMISSIONS;
import static ee.ut.sepp.natureapp.constants.PermissionsConstants.PERMISSIONS_GROUP;

public class NewGroupActivity extends AppCompatActivity {

    @BindView(R.id.members_layout)
    RelativeLayout membersLayout;
    @BindView(R.id.group_name_layout)
    TextInputLayout groupNameLayout;
    @BindView(R.id.group_name)
    TextInputEditText groupName;
    @BindView(R.id.group_photo)
    CircleImageView groupPhoto;

    @BindView(R.id.inputs_holder)
    LinearLayout mainParentLayout;

    private List<TextInputLayout> memberLayoutList = new ArrayList<>();
    private String imageResource;
    String classId;
    AppDatabase db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);

        ButterKnife.bind(this);

        if (getIntent().hasExtra(CLASS_ID)) {
            classId = getIntent().getStringExtra(CLASS_ID);
        }
        db = AppDatabase.getAppDatabase(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return false;
    }

    @OnClick(R.id.btn_create_new_group)
    void createNewGroup() {
        if (processValidation()) {
            DialogHelper.showProgressDialog(NewGroupActivity.this);
            checkExistingGroupName(new GroupNameCallback() {
                @Override
                public void checkExistingName(boolean exists) {
                    if (exists) {
                        groupNameLayout.setErrorEnabled(true);
                        groupNameLayout.setError(getString(R.string.existing_group_name));
                    } else {
                        FirebaseHelper.saveNewGroup(NewGroupActivity.this, ModelFactory.createGroup(null, classId, groupNameLayout, imageResource, memberLayoutList));
                        Toast.makeText(NewGroupActivity.this, getString(R.string.success_new_group), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(NewGroupActivity.this, StartGameActivity.class));
                    }
                    DialogHelper.dismissProgressDialog();
                }
            }, groupName.getText().toString());

        }

    }

    @OnClick(R.id.btn_add_new_member)
    void addNewMember() {
        generateNewInput();
    }

    @OnClick(R.id.group_photo)
    void chooseGroupPhoto() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSIONS_GROUP);
            return;
        }
       ImagePicker.pickImage(this, getString(R.string.choose_group_photo));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);

        if (bitmap != null) {
            groupPhoto.setImageBitmap(bitmap);
            imageResource = ImageHelper.encodeBitmap(bitmap);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_GROUP: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(NewGroupActivity.this, "Permission was granted", Toast.LENGTH_LONG).show();

                }
            }
        }
    }

    /**
     * Checks if all inputs are valid. If not, shows validation messages
     *
     * @return true if validation is valid, otherwise false
     */
    private boolean processValidation() {
        clearAllInputs();
        final boolean[] isValid = {true};
        if (groupName.getText().toString().isEmpty()) {
            groupNameLayout.setErrorEnabled(true);
            groupNameLayout.setError(getString(R.string.error_no_group_name));
            isValid[0] = false;
        }

        if (memberLayoutList.size() == 0) {
            addNewMember();
            if (memberLayoutList.size() == 1) {
                memberLayoutList.get(0).setError(getString(R.string.error_no_member_name));
                isValid[0] = false;
            }
        } else {
            for (TextInputLayout textInputLayout : memberLayoutList) {
                if (textInputLayout.getEditText().getText().toString().isEmpty()) {
                    textInputLayout.setError(getString(R.string.error_no_member_name));
                    if (isValid[0]) isValid[0] = false;
                }
            }
        }


        return isValid[0];
    }


    private void checkExistingGroupName(@NonNull final GroupNameCallback finishedCallback, String groupName) {
        final DatabaseReference reference = FirebaseHelper.getDatabaseReference();
        Query query = reference.child(GROUP_NODE).orderByChild(GROUP_NAME).equalTo(groupName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    finishedCallback.checkExistingName(false);
                    return;
                }
                boolean theSameClass = false;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String retrievedClassId = (String) snapshot.child(CLASS_ID).getValue();
                    if (snapshot.exists() && retrievedClassId != null && retrievedClassId.equals(SharedPrefencesHelper.readCurrentClassId(NewGroupActivity.this))) {
                        finishedCallback.checkExistingName(true);
                        theSameClass = true;
                    }
                }

                if (!theSameClass) {
                    finishedCallback.checkExistingName(false);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public interface GroupNameCallback {
        void checkExistingName(boolean exists);
    }

    /**
     * Clears all validation messages
     */
    private void clearAllInputs() {
        groupNameLayout.setError(null);
        groupNameLayout.setErrorEnabled(false);
        for (TextInputLayout textInputLayout : memberLayoutList) {
            textInputLayout.setErrorEnabled(false);
            textInputLayout.setError(null);
        }
    }

    /**
     * Generates a new input for adding a group member dynamically
     */
    private void generateNewInput() {
        final TextInputLayout inputLayout = new TextInputLayout(this);
        final TextInputEditText inputEditText = new TextInputEditText(this);
        inputEditText.setMaxLines(1);
        inputEditText.setInputType(InputType.TYPE_CLASS_TEXT);
        inputEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_clear, 0);

        inputEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (inputEditText.getRight() - inputEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        removeMemberInput((TextInputLayout) inputEditText.getParent().getParent());
                        return true;
                    }
                }
                return false;
            }
        });

        LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        inputEditText.setLayoutParams(editTextParams);


        RelativeLayout.LayoutParams textInputLayoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        if (!memberLayoutList.isEmpty()) {
            textInputLayoutParams.addRule(RelativeLayout.BELOW, memberLayoutList.get(memberLayoutList.size() - 1).getId());
        }
        inputLayout.setLayoutParams(textInputLayoutParams);

        memberLayoutList.add(inputLayout);
        inputLayout.setId(memberLayoutList.indexOf(inputLayout) + 1);
        inputLayout.setHint(getString(R.string.add_new_member) + " " + inputLayout.getId());

        inputLayout.addView(inputEditText, editTextParams);
        membersLayout.addView(inputLayout, textInputLayoutParams);

    }

    private void removeMemberInput(TextInputLayout textInputLayout) {
        int start = memberLayoutList.indexOf(textInputLayout);
        memberLayoutList.remove(textInputLayout);
        textInputLayout.removeAllViews();

        if (!memberLayoutList.isEmpty()) {
            for (int i = start; i < memberLayoutList.size(); i++) {
                TextInputLayout inputLayout = memberLayoutList.get(i);
                inputLayout.setId(i + 1);
                inputLayout.setHint(getString(R.string.add_new_member) + " " + inputLayout.getId());
                ((RelativeLayout.LayoutParams) inputLayout.getLayoutParams()).removeRule(RelativeLayout.BELOW);
                if (i != 0) {
                    ((RelativeLayout.LayoutParams) inputLayout.getLayoutParams()).addRule(RelativeLayout.BELOW, memberLayoutList.get(i - 1).getId());
                }

            }
        }
    }
}
