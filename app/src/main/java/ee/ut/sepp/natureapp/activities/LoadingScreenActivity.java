package ee.ut.sepp.natureapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.helpers.FirebaseHelper;
import ee.ut.sepp.natureapp.helpers.ImageHelper;
import ee.ut.sepp.natureapp.helpers.SharedPrefencesHelper;
import ee.ut.sepp.natureapp.models.Group;
import ee.ut.sepp.natureapp.models.Task;

import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_CLASS_ID;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_MEMBERS;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_NAME;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_NODE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_PHOTO;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.GROUP_UNIQUE_ID;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.TASK_DESC;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.TASK_ID;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.TASK_IN_CLASS_NODE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.TASK_NODE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.TASK_PICTURE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.TASK_TITLE;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.CLASS_NAME;
import static ee.ut.sepp.natureapp.constants.SettingsConstants.LOADING_SCREEN_LENGTH;

public class LoadingScreenActivity extends AppCompatActivity {
    @BindView(R.id.loading_info)
    TextView loadingText;

    @BindView(R.id.loading_picture)
    ImageView loadingPicture;

    AppDatabase db;
    String classId, className;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);

        ButterKnife.bind(this);

        db = AppDatabase.getAppDatabase(this);

        if (getIntent().hasExtra(CLASS_ID) && getIntent().hasExtra(CLASS_NAME)) {
            classId = getIntent().getStringExtra(CLASS_ID);
            className = getIntent().getStringExtra(CLASS_NAME);
            loadGroupData(this);
            preloadData();
        }

        RotateAnimation rotate = new RotateAnimation(
                0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
        );
        rotate.setDuration(500);
        rotate.setStartOffset(1000);
        rotate.setRepeatCount(Animation.INFINITE);
        loadingPicture.startAnimation(rotate);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LoadingScreenActivity.this, ViewGroupsActivity.class);
                intent.putExtra(CLASS_ID, classId);
                intent.putExtra(CLASS_NAME, className);
                startActivity(intent);
                finish();
            }
        }, LOADING_SCREEN_LENGTH);


    }

    private void loadGroupData(final Context context) {
        final ArrayList<String> members = new ArrayList<>();
        DatabaseReference reference = FirebaseHelper.getDatabaseReference();

        Query query = reference.child(GROUP_NODE);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Group group;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot.child(GROUP_CLASS_ID).getValue() != null) {
                        if (snapshot.child(GROUP_CLASS_ID).getValue().toString().equals(classId)) {
                            if (snapshot.child(GROUP_PHOTO).exists() && snapshot.child(GROUP_MEMBERS).exists()) {
                                members.clear();
                                for (int i = 0; i < snapshot.child(GROUP_MEMBERS).getChildrenCount(); i++) {
                                    members.add(snapshot.child(GROUP_MEMBERS).child(String.valueOf(i)).getValue().toString());
                                }
                                group = new Group(snapshot.child(GROUP_UNIQUE_ID).getValue().toString(),
                                        snapshot.child(GROUP_CLASS_ID).getValue().toString(),
                                        snapshot.child(GROUP_NAME).getValue().toString(),
                                        snapshot.child(GROUP_PHOTO).getValue().toString(),
                                        members);


                            } else if (snapshot.child(GROUP_MEMBERS).exists()) {
                                members.clear();
                                for (int i = 0; i < snapshot.child(GROUP_MEMBERS).getChildrenCount(); i++) {
                                    members.add(snapshot.child(GROUP_MEMBERS).child(String.valueOf(i)).getValue().toString());
                                }
                                group = new Group(snapshot.child(GROUP_UNIQUE_ID).getValue().toString(),
                                        snapshot.child(GROUP_CLASS_ID).getValue().toString(),
                                        snapshot.child(GROUP_NAME).getValue().toString(),
                                        ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(context, R.drawable.photo_placeholder)),
                                        members);
                            } else if (snapshot.child(GROUP_PHOTO).exists()) {
                                group = new Group(snapshot.child(GROUP_UNIQUE_ID).getValue().toString(),
                                        snapshot.child(GROUP_CLASS_ID).getValue().toString(),
                                        snapshot.child(GROUP_NAME).getValue().toString(),
                                        snapshot.child(GROUP_PHOTO).getValue().toString(),
                                        new ArrayList<String>());
                            } else {
                                group = new Group(snapshot.child(GROUP_UNIQUE_ID).getValue().toString(),
                                        snapshot.child(GROUP_CLASS_ID).getValue().toString(),
                                        snapshot.child(GROUP_NAME).getValue().toString(),
                                        ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(context, R.drawable.photo_placeholder)),
                                        new ArrayList<String>());
                            }
                            db.groupDao().insertNewGroup(group);


                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void preloadData() {
        final DatabaseReference reference = FirebaseHelper.getDatabaseReference();

        Query query = reference.child(TASK_IN_CLASS_NODE).child(SharedPrefencesHelper.readCurrentClassId(LoadingScreenActivity.this));

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    readTaskFromFirebase(snapshot.getKey(), reference);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

//    private void prepareTopics() {
//        List<Topic> mockTopics = new ArrayList<>();
//        mockTopics.add(new Topic("1", "Trees", TopicStatus.DONE.name(), ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(this, R.drawable.tree))));
//        mockTopics.add(new Topic("2", "Animal", TopicStatus.IN_PROGRESS.name(), ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(this, R.drawable.animal))));
//        mockTopics.add(new Topic("3", "Insect", TopicStatus.DONE.name(), ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(this, R.drawable.insect))));
//        mockTopics.add(new Topic("4", "Flower", TopicStatus.IN_PROGRESS.name(), ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(this, R.drawable.flower))));
//
//        for (Topic topic : mockTopics) {
//            db.topicDao().insertNewTopic(topic);
//        }
//    }

    private void readTaskFromFirebase(String taskId, DatabaseReference databaseReference) {
        Query query = databaseReference.child(TASK_NODE).orderByKey().equalTo(taskId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Task task;
                // todo cover cases when photos are separated by comma
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String classId = SharedPrefencesHelper.readCurrentClassId(LoadingScreenActivity.this);
                    if (!snapshot.child(TASK_PICTURE).exists()) {
                        task = new Task(snapshot.child(TASK_ID).getValue().toString(), classId, snapshot.child(TASK_TITLE).getValue().toString(), getString(R.string.subtitle),
                                snapshot.child(TASK_DESC).getValue().toString(),
                                ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(LoadingScreenActivity.this, R.drawable.no_image_placeholder)));
                    } else {
                        String[] split = snapshot.child(TASK_PICTURE).getValue().toString().split(",");
                        if (split.length == 2) {
                            task = new Task(snapshot.child(TASK_ID).getValue().toString(), classId, snapshot.child(TASK_TITLE).getValue().toString(), getString(R.string.subtitle),
                                    snapshot.child(TASK_DESC).getValue().toString(),
                                    split[1]);
                        } else
                            task = new Task(snapshot.child(TASK_ID).getValue().toString(), classId, snapshot.child(TASK_TITLE).getValue().toString(), getString(R.string.subtitle),
                                    snapshot.child(TASK_DESC).getValue().toString(),
                                    ImageHelper.encodeBitmap(ImageHelper.drawableToBitmap(LoadingScreenActivity.this, R.drawable.no_image_placeholder)));
                    }
                    db.taskDao().insertNewTask(task);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
