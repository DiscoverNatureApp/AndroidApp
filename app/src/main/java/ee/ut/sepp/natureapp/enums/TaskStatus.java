package ee.ut.sepp.natureapp.enums;

import ee.ut.sepp.natureapp.R;

public enum TaskStatus {
    PENDING(R.drawable.pending),
    DONE(R.drawable.checked);

    private int imageResource;
    TaskStatus(int imageResource) {
        this.imageResource = imageResource;
    }

    public int getImageResource() {
        return imageResource;
    }
}
