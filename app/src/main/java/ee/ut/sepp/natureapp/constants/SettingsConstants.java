package ee.ut.sepp.natureapp.constants;


public class SettingsConstants {
    public static final int SPLASH_DISPLAY_LENGTH = 1000;
    public static final int LOADING_SCREEN_LENGTH = 6000;
    public static final String CUSTOM_FONT = "RibeyeMarrowRegular.ttf";
}
