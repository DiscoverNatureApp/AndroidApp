package ee.ut.sepp.natureapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import ee.ut.sepp.natureapp.models.Topic;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface TopicDao {
    @Query("SELECT * FROM topics")
    List<Topic> getAllTopic();

    @Query("SELECT * FROM topics WHERE id = :topicId")
    Topic getTopicById(String topicId);

    @Insert(onConflict = REPLACE)
    void insertNewTopic(Topic topic);
}
