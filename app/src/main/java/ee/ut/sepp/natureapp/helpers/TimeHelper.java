package ee.ut.sepp.natureapp.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by admin on 08.12.2017.
 */

public class TimeHelper {
    public static String getFormattedTime(long millisUntilFinished) {
        Date date = new Date(millisUntilFinished);
        DateFormat formatter = new SimpleDateFormat("mm:ss");

        return formatter.format(date);
    }
}
