package ee.ut.sepp.natureapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.models.Group;
import ee.ut.sepp.natureapp.viewholders.GroupViewHolder;

public class GroupsAdapter extends RecyclerView.Adapter<GroupViewHolder>{

    private List<Group> groupsList;
    private Context context;

    public GroupsAdapter(Context context, List<Group> groupsList){
        this.groupsList = groupsList;
        this.context = context;
    }

    public Object getItem(int location) {
        return groupsList.get(location);
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View groupView = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_row, parent, false);

        return new GroupViewHolder(context, groupView);
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        Group group = groupsList.get(position);
        holder.onBind(group);
    }

    @Override
    public int getItemCount(){
        return groupsList.size();
    }

    public void setFilter(List<Group> groups) {
        groupsList = new ArrayList<>();
        groupsList.addAll(groups);
        notifyDataSetChanged();
    }

}
