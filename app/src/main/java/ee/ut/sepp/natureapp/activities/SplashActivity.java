package ee.ut.sepp.natureapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.helpers.FontHelper;

import static ee.ut.sepp.natureapp.constants.SettingsConstants.CUSTOM_FONT;
import static ee.ut.sepp.natureapp.constants.SettingsConstants.SPLASH_DISPLAY_LENGTH;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    @BindView(R.id.app_name)
    TextView appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();

            }
        }, SPLASH_DISPLAY_LENGTH);

        FontHelper.setFont(this, appName, CUSTOM_FONT);

    }
}
