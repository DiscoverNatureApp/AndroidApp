package ee.ut.sepp.natureapp.activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import ee.ut.sepp.natureapp.R;
import ee.ut.sepp.natureapp.db.AppDatabase;
import ee.ut.sepp.natureapp.helpers.FirebaseHelper;
import ee.ut.sepp.natureapp.helpers.ImageHelper;
import ee.ut.sepp.natureapp.models.Group;
import ee.ut.sepp.natureapp.models.Result;

import static android.util.Base64.DEFAULT;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.ANSWER_GRADE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.ANSWER_GRADING_FINISHED;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.ANSWER_NODE;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.ANSWER_RESULTS;
import static ee.ut.sepp.natureapp.constants.FirebaseConstants.ANSWER_TOTAL_SCORE;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.GROUP_ID;
import static ee.ut.sepp.natureapp.constants.GeneralConstants.IS_GROUP;

//import ee.ut.sepp.natureapp.models.Group;

public class GroupsDetailActivity extends AppCompatActivity {
    @BindView(R.id.group_detail_name)
    TextView groupName;
    @BindView(R.id.group_points)
    TextView groupPoints;
    @BindView(R.id.group_picture)
    CircleImageView groupPicture;
    @BindView(R.id.members_listview)
    ListView listView;

    Group group;
    AppDatabase db;
    private String groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups_detail);

        ButterKnife.bind(this);

        db = AppDatabase.getAppDatabase(this);

        if (getIntent().hasExtra(GROUP_ID)) {
            groupId = getIntent().getStringExtra(GROUP_ID);
            preloadGroupPictures();
            prepareContent(groupId);
        }
    }

    @OnClick(R.id.view_submitted_results)
    public void viewResults() {
        finish();
        Intent intent = new Intent(GroupsDetailActivity.this, TasksActivity.class);
        intent.putExtra(IS_GROUP, true);
        intent.putExtra(GROUP_ID, group.getId());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return false;
    }

    public void preloadGroupPictures(){
        DatabaseReference reference = FirebaseHelper.getDatabaseReference();

        Query query = reference.child(ANSWER_NODE).orderByKey().equalTo(groupId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(groupId).exists() && dataSnapshot.child(groupId).child(ANSWER_RESULTS).exists()){
                    String[] pieces = dataSnapshot.child(groupId).child(ANSWER_RESULTS).getValue().toString().replaceAll("[\\s\\n{}]","").split(",|=");
                    for (int i = 0; i < pieces.length; i++){
                        if (pieces[i].contains("-")) {
                            int totalScore = ((Long) dataSnapshot.child(groupId).child(ANSWER_TOTAL_SCORE).getValue()).intValue();
                            boolean gradedFinished = (boolean) dataSnapshot.child(groupId).child(ANSWER_GRADING_FINISHED).getValue();
                            String rightOrWrong = "";
                            if (dataSnapshot.child(groupId).child(ANSWER_GRADE).exists()) {
                                for (DataSnapshot snapshot : dataSnapshot.child(groupId).child(ANSWER_GRADE).getChildren()) {
                                    if (snapshot.getKey().equals(pieces[i])) {
                                        rightOrWrong = snapshot.getValue().toString();
                                        break;
                                    }
                                }
                            }
                            db.resultDao().insertNewResult(new Result(pieces[i+1], pieces[i], groupId,
                                    totalScore, gradedFinished, rightOrWrong));
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void prepareContent(String groupId) {
        group = db.groupDao().getGroupById(groupId);
        if (group != null) {
            setTitle("Group " + group.getName());
            groupName.setText(group.getName());
            groupPoints.setText(String.valueOf(AppDatabase.getAppDatabase(this).resultDao().getTotalScoreOfGroup(group.getId())));
            try{
                byte[] data = Base64.decode(group.getPhoto(), DEFAULT);
                Glide.with(this).load(BitmapFactory.decodeByteArray(data,0, data.length)).into(groupPicture);
            } catch(Exception e){
                Glide.with(this).load(ImageHelper.decodeBitmap(group.getPhoto())).into(groupPicture);
            }
            finally {
                listView.setAdapter(new ArrayAdapter<>(this, R.layout.member_name_row, R.id.member_name, group.getMembers()));
            }
        }
    }


}
