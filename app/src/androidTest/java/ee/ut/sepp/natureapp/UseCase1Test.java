package ee.ut.sepp.natureapp;

import android.os.SystemClock;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import ee.ut.sepp.natureapp.activities.MyClassesActivity;
import ee.ut.sepp.natureapp.activities.NewGroupActivity;
import ee.ut.sepp.natureapp.activities.TaskTopicsActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class UseCase1Test {

    @Rule
    public ActivityTestRule<MyClassesActivity> activityTestRule = new ActivityTestRule<>(MyClassesActivity.class, false, false);


    @Test
    public void JoinNewClassSuccessfully() {
        Intents.init();
        activityTestRule.launchActivity(null);
        onView(withId(R.id.btn_show_joining_form)).perform(click());

        joinClass();
        createGroupAndStart(true);

        //checks whether the intent was launched or not
        intended(hasComponent(TaskTopicsActivity.class.getName()));
        Intents.release();

    }

    @Test
    public void JoinNewClassWithBadAttempts() {
        Intents.init();
        activityTestRule.launchActivity(null);
        onView(withId(R.id.btn_show_joining_form)).perform(click());

        //try joining new class
        onView(withId(R.id.class_code))
                .perform(typeText("aslkjsoighnmnsiu"), closeSoftKeyboard());
        onView(withId(R.id.btn_join_class)).perform(click());

        SystemClock.sleep(3000);

        //code was wrong
        onView(withText(R.string.wrong_group_code)).check(matches(isDisplayed()));

        onView(withId(R.id.class_code)).perform(clearText());
        joinClass();

        //try (and fail) continuing without inserting member/group name
        onView(withId(R.id.btn_add_new_member)).perform(click());

        onView(withId(R.id.btn_create_new_group)).perform(click());

        onView(withText(R.string.error_no_group_name)).check(matches(isDisplayed()));
        onView(withText(R.string.error_no_member_name)).check(matches(isDisplayed()));

        createGroupAndStart(false);

        intended(hasComponent(NewGroupActivity.class.getName()));
        Intents.release();
    }


    public String random() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(10);
        if (randomLength == 0) {
            randomLength = 10;
        }
        for (int i = 0; i < randomLength; i++) {
            char c = chars[generator.nextInt(chars.length)];
            randomStringBuilder.append(c);
        }
        return randomStringBuilder.toString();
    }

    public void joinClass() {
        onView(withId(R.id.class_code))
                .perform(typeText("mhm"), closeSoftKeyboard());
        onView(withId(R.id.btn_join_class)).perform(click());

        //wait for 3 seconds so the database could do its work and test wouldn't rush
        SystemClock.sleep(3000);

    }

    public void createGroupAndStart(boolean create) {
        //name a group
        onView(withId(R.id.group_name)).perform(typeText(random()), closeSoftKeyboard());

        //add a member
        if (create) {
            onView(withId(R.id.btn_add_new_member)).perform(click());
        }

        String name = random();

        onView(withText("")).perform(click(), typeText(name));
        onView(withText(name)).perform(closeSoftKeyboard());
        SystemClock.sleep(1000);

        //create group
        onView(withId(R.id.btn_create_new_group)).perform(click());

        //start game
        onView(withId(R.id.btn_show_start)).perform(click());

    }
}
