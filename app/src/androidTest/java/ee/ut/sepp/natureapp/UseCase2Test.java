package ee.ut.sepp.natureapp;

import android.os.SystemClock;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ee.ut.sepp.natureapp.activities.StartGameActivity;
import ee.ut.sepp.natureapp.activities.TasksActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class UseCase2Test {
    @Rule
    public ActivityTestRule<StartGameActivity> activityTestRule = new ActivityTestRule<>(StartGameActivity.class,
            false, false);

    @Test
    public void ViewTasksInTopic() {
        Intents.init();

        activityTestRule.launchActivity(null);

        SystemClock.sleep(2000);
        onView(withId(R.id.btn_show_start)).perform(click());

        SystemClock.sleep(2000);

        onView(withText("Trees")).perform(click());

        //give it time to load in the tasks
        SystemClock.sleep(5000);


        //check if there is a list of tasks on screen
        onView(withId(R.id.tasks_view)).check(matches(isDisplayed()));

        //checks whether the intent was launched or not
        intended(hasComponent(TasksActivity.class.getName()));
        Intents.release();

    }

}
