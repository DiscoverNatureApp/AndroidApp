# Mobile App for Learning Nature

## The Goal

Encouraging physical movement, observational skills and knowledge about nature in students.

## Description

The mobile application has collections of descriptions and images of common natural objects (plants, insects etc). Students’ goal is to find the specific objects from nature, take a imageDesc of the object and mark the imageDesc with a label of what they think the object is. Other users can then vote if they also think the object is what the author thought it was. Users gain score for taking photos and voting. Depending on the user’s location there are different collections of objects for users to find, for example: all the common trees in Estonia.


## Running instructions

#### Android application
By default Android does not allow a user to install applications that are not on the Google Play Store. To run the app it is necessary to allow the installation of unknown sources (“Settings” > “Security” > check “Unknown Sources”).

The APK file can be downloaded from provided [link](https://gitlab.com/DiscoverNatureApp/mobileNatureApp/wikis/Release-notes) for particular iteration  and transfered to a device via USB or download it directly to a phone.

Also it is possible to clone our Android repository and build the app in Android Studio. After that you can run it on a real device or an emulator (See an official tutorial). 

